
/**
 * @file
 * Admin javascript.
 */

Drupal.behaviors.WysiwygCleaner = function() {

	var container = $('#wysiwyg_cleaner');
	var previewMode = 'html';
	var previewBefore = $('.preview-before .wrapper', container).html();
	var previewAfter;
	
	// Hide all empty rows
	$('.row', container).each(function() {
		if ($('.rule', $(this)).val() == '') {
			$(this).hide();
		}
	});
	
	// Show first row if there is no visible rows
	if ($('.row:visible', container).size() == 0) {
		$('.row:first', container).show();
	}
	
	// Hide all custom values and arguments
	$('.row .custom, .row .wrapper-arguments', container).hide();
		
	// Show existing custom values and arguments
	$('.row', container).each(function() {
		var value = $('.method', $(this)).val();
		if (value == 'custom') {
			$('.custom', $(this)).show();
			if ($('.rule', $(this)).val().charAt(0) == '/') {
				$('.method', $(this)).attr('disabled', true);
			}
		}
		else if (value.match(/%[s|d|f|%]/)) {
			$('.wrapper-arguments', $(this)).show();
		}
	});
	
	// Toggle custom values
	$('.row .method', container).change(function() {
		var value = $(this).val();
		var context = $(this).parents('.wrapper');
		if (value == 'custom') {
			$('.custom',  context).show();
			$('.wrapper-arguments',  context).hide();
		}
		else {
			$('.custom',  context).hide();
			if (value.match(/%[s|d|f|%]/)) {
				$('.wrapper-arguments',  context).show();
			}
			else {
				$('.wrapper-arguments',  context).hide();
			}
		}
	});
	
	// Toggle for regexps
	$('.row .rule', container).keyup(function() {
		var rule = $(this).val();
		if (rule.charAt(0) == '/') {
			var context = $(this).parents('.wrapper').next();
			$('.method', context).attr('disabled', true);
			$('.method', context).val('custom');
			$('.custom', context).show();
			$('.wrapper-arguments', context).hide();
		}
		else {
			$('.method', context).removeAttr('disabled');
		}
	});
	
	// Show JS links
	$('a.link-add-rule, a.link-edit-preview, a.link-switch-preview, a.link-delete-rule', container).show();
	
	// Delete a row
	$('a.link-delete-rule', container).click(function() {
		$(this).parents('.row').hide();
		$('.rule, .method, .custom', $(this).parents('.row')).val('');
	});
	
	// Add a row
	$('a.link-add-rule', container).click(function() {
		$('.row:visible:last', container).next('.row').show('fast');
	});
	
	// Hide preview markup editor
	$('.edit-preview', container).hide();
	
	// Toggle preview markup editor
	$('a.link-edit-preview', container).click(function() {
		$('.edit-preview', container).toggle('fast');
	});
	
	// Toggle preview markup editor
	$('a.link-switch-preview', container).click(function() {
		if (previewMode == 'source') {
			previewMode = 'html';
		}
		else {
			previewMode = 'source';
		}
		updatePreview();
	});
	
	// Edit preview markup
	$('.edit-preview', container).change(function() {
		previewBefore = $('textarea', this).val();
		previewAfter = '';
		updatePreview();
	});
	
	// Preview
	$('input.wysiwyg_cleaner_preview_submit', container).click(function() {
		
		$('.edit-preview', container).hide('fast');
		
		var preset = { rules : {} };
		
		$('.row', container).each(function(i) {
			var rule = $('.rule', this).val();
			if (rule) {
				var value = $('.method', this).val();
				var arguments = $('.arguments', this).val();
				if (value == 'custom') {
					value = $('.custom', this).val();
				}
				preset.rules[i] = { rule : rule, value : value, arguments : arguments };
			}
		});
		
		var before = $('.preview-before .wrapper', container);
		var after = $('.preview-after .wrapper', container);
		before.html($('.edit-preview textarea', container).val());
		previewBefore = before.html();
		after.html(previewBefore);
		
		try {
			Drupal.Wysiwyg.Cleaner(after, preset, true);
		}
		catch(e) {
			alert(e.message);
		}
		
		previewAfter = after.html();
		updatePreview();
		
		return false;
	});
	
	updatePreview = function() {
		if (previewMode == 'source') {
			$('.preview-before .wrapper').text(previewBefore);
			$('.preview-after .wrapper').text(previewAfter);
		}
		else {
			$('.preview-before .wrapper').html(previewBefore);
			$('.preview-after .wrapper').html(previewAfter);
		}
	};

};