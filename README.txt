
Wysiwyg cleaner
---------------
Allows users to clean up content in wysiwyg editors.
Using rules based on jQuery selectors or regular expression, you can provide buttons for wysiwyg editors to clean up content. 
Optionally, cleaning can be automatically fired on paste.

Installation
------------
Ensure you have the the last 6.x-2.x-dev version of jQuery Update module (http://drupal.org/project/jquery_update).
Unzip the module into your modules directory.
Enable the module.

Usage
-----

* Profiles

Navigate to admin/settings/wysiwyg/cleaner.
You will see the provided default presets, for now, the only profile is "Open Office".

* Activate a button

To activate a button, navigate to admin/settings/wysiwyg and edit a profile.
In "Buttons and plugins", check the wanted Wysiwyg cleaner buttons.

* Profile creation

Navigate to admin/settings/wysiwyg/cleaner/preset/add.
Each rule can be either a regular expression or a jQuery selector.
See the "Rules" section for a description of rules.
Add as many rules as you want.
You can see a preview using the "Preview" button. 
Be sure to have set a preview markup using the "Change preview markup" link for preview to work.

* Rules

- jQuery :
	The syntax is : li > p
	Replace values :
		- Remove tag : The tag is stripped but its content remains.
		- Delete : The tag and its content are removed.
		- Remove all attributes : Removes all tag attributes.
		- Change tag : Changes the tag.

- Regular expressions :
	The syntax is : /[a-z]+[0-9]*/g
	The replace value will be used as the second argument in a replace() function. Leave empty to remove.

Maintainers
-----------
Julien De Luca