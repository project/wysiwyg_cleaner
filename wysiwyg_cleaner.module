<?php

/**
 * @file
 * Allows users to clean up content in wysiwyg editor.
 *
 * @todo : 
 * - Bug when creating a rule with selector, then save, then edit again and change to regexp and let it empty. Edit again : it is a function. 
 * - Only add wysiwyg_cleaner_presets if needed.
 * - Use inc for presets.
 * - Pass coder.
 * - Weights and reorder -> consequently use a table.
 * - Separate js tool function in another js file ?
 */

/**
 * Implementation of hook_init().
 */
function wysiwyg_cleaner_init() {
	// We can't use drupal_add_js as setting here since it would not be available for the Wysiwyg API plugin.
  drupal_set_html_head('<script type="text/javascript">var wysiwyg_cleaner_presets = '.drupal_to_js(wysiwyg_cleaner_presets_get_pids()).'</script>');
  drupal_add_js(drupal_get_path('module', 'wysiwyg_cleaner') .'/wysiwyg_cleaner.api.js');
}

/**
 * Implementation of hook_wysiwyg_include_directory().
 */
function wysiwyg_cleaner_wysiwyg_include_directory($type) {
	if ($type == 'plugins') {
		return 'plugin';
	}
}

/**
 * Implementation of hook_menu().
 */
function wysiwyg_cleaner_menu() {
  $items = array();

  $items['admin/settings/wysiwyg/cleaner'] = array(
    'title' => t('Wysiwyg cleaner'),
    'description' => t('Allows users to paste clean content in wysiwyg editor'),
    'page callback' => 'wysiwyg_cleaner_admin',
    'access arguments' => array('administer filters'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'wysiwyg_cleaner.admin.inc',
  );
  
  $items['admin/settings/wysiwyg/cleaner/presets'] = array(
    'title' => t('Presets'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  
  $items['admin/settings/wysiwyg/cleaner/preset/add'] = array(
    'title' => t('Add Preset'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wysiwyg_cleaner_admin_preset_form'),
    'access arguments' => array('administer filters'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'wysiwyg_cleaner.admin.inc',
  );
  
  $items['admin/settings/wysiwyg/cleaner/preset/%/edit'] = array(
    'title' => t('Edit Preset'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wysiwyg_cleaner_admin_preset_form', 5),
    'access arguments' => array('administer filters'),
    'type' => MENU_CALLBACK,
    'file' => 'wysiwyg_cleaner.admin.inc',
  );
  
  $items['admin/settings/wysiwyg/cleaner/preset/%/delete'] = array(
    'title' => t('Edit Preset'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wysiwyg_cleaner_admin_preset_delete_form', 5),
    'access arguments' => array('administer filters'),
    'type' => MENU_CALLBACK,
    'file' => 'wysiwyg_cleaner.admin.inc',
  );
  
  $items['admin/settings/wysiwyg/cleaner/preset/%/export'] = array(
    'title' => t('Export Preset'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wysiwyg_cleaner_admin_preset_export_form', 5),
    'access arguments' => array('administer filters'),
    'type' => MENU_CALLBACK,
    'file' => 'wysiwyg_cleaner.admin.inc',
  );
  
  $items['admin/settings/wysiwyg/cleaner/import'] = array(
    'title' => t('Import'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('wysiwyg_cleaner_admin_import_form'),
    'access arguments' => array('administer filters'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'wysiwyg_cleaner.admin.inc',
  );

  return $items;
}

/**
 * CRUD function. Get all presets.
 *
 * @return
 *    All matched presets in a keyed array.
 */
function wysiwyg_cleaner_presets_get_all() {
	$presets = array();
	$query = db_query("SELECT * FROM {wysiwyg_cleaner_presets}");
	while ($preset = db_fetch_object($query)) {
		$presets[$preset->pid] = $preset;
		$presets[$preset->pid]->rules = wysiwyg_cleaner_rules_get($preset->pid);
	}
  return $presets;
}

/**
 * CRUD function. Get all presets ids.
 *
 * @return
 *    An array with all pids.
 */
function wysiwyg_cleaner_presets_get_pids() {
	$presets = array();
	$query = db_query("SELECT pid FROM {wysiwyg_cleaner_presets}");
	while ($preset = db_fetch_object($query)) {
		$presets[] = $preset->pid;
	}
  return $presets;
}

/**
 * CRUD function. Get a preset
 *
 * @param pid
 *    The preset pid.
 * @return
 *    The preset object.
 */
function wysiwyg_cleaner_preset_get($pid) {
	if (!$pid) {
		return;
	}
	$preset = db_fetch_object(db_query("SELECT * FROM {wysiwyg_cleaner_presets} WHERE pid = %d LIMIT 1", $pid));
	$preset->rules = wysiwyg_cleaner_rules_get($pid);
  return $preset;
}

/**
 * CRUD function. Get rules of a preset.
 *
 * @param pid
 *    The preset pid.
 * @return
 *    All matched rules in a keyed array.
 */
function wysiwyg_cleaner_rules_get($pid) {
	if (!$pid) {
		return;
	}
	$rules = array();
	$query = db_query("SELECT * FROM {wysiwyg_cleaner_rules} WHERE pid = %d ORDER BY rid", $pid);
	while ($rule = db_fetch_object($query)) {
		$rules[$rule->rid] = $rule;
	}
  return $rules;
}

/**
 * CRUD function. Add or update a preset.
 *
 * @param preset
 *    The preset object.
 * @return
 *    The preset object with its pid.
 */
function wysiwyg_cleaner_preset_save($preset) {
	if (!$preset->pid) {
		db_query("INSERT INTO {wysiwyg_cleaner_presets} (name, icon) VALUES ('%s', '%s')", $preset->name, $preset->icon);
		$preset->pid = db_last_insert_id('wysiwyg_cleaner_presets', 'pid');
	}
  else {
  	db_query("UPDATE {wysiwyg_cleaner_presets} SET name = '%s', icon = '%s' WHERE pid = %d", $preset->name, $preset->icon, $preset->pid);
  }
  foreach ($preset->rules as $rule) {
  	$rule->pid = $preset->pid;
  	$rule = wysiwyg_cleaner_rule_save($rule);
  	$preset->rules[$rule->rid] = $rule;
  }
  return $preset;
}

/**
 * CRUD function. Delete a preset.
 *
 * @param pid
 *    The preset pid.
 */
function wysiwyg_cleaner_preset_delete($pid) {
	db_query("DELETE FROM {wysiwyg_cleaner_presets} WHERE pid = %d", $pid);
	db_query("DELETE FROM {wysiwyg_cleaner_rules} WHERE pid = %d", $pid);
}

/**
 * CRUD function. Add or update a rule.
 *
 * @param rule
 *    The rule object.
 * @return
 *    The rule object with its pid.
 */
function wysiwyg_cleaner_rule_save($rule) {
	if (!$rule->rid) {
		db_query("INSERT INTO {wysiwyg_cleaner_rules} (rule, value, arguments, pid) VALUES ('%s', '%s', '%s', %d)", $rule->rule, $rule->value, $rule->arguments, $rule->pid);
		$rule->rid = db_last_insert_id('wysiwyg_cleaner_rules', 'rid');
	}
  else if ($rule->rule) {
  	$arguments = _wysiwyg_cleaner_get_arguments($rule);
  	$cards = '"'.join(',', $arguments['cards']).'"';
  	db_query("UPDATE {wysiwyg_cleaner_rules} SET arguments = $cards, rule = '%s', value = '%s', pid = %d WHERE rid=%d", $arguments['query_args']);
  }
  else {
  	wysiwyg_cleaner_rule_delete($rule->rid);
  	$rule = NULL;
  }
  return $rule;
}

/**
 * CRUD function. Delete a rule.
 *
 * @param rid
 *    The rule rid.
 */
function wysiwyg_cleaner_rule_delete($rid) {
	db_query("DELETE FROM {wysiwyg_cleaner_rules} WHERE rid = %d", $rid);
}

/**
 * Import/Export function. Create an exportable string.
 *
 * @param pid
 *    The preset id.
 * @return
 *    An exportable string.
 */
function wysiwyg_cleaner_preset_export($pid) {
	$preset = wysiwyg_cleaner_preset_get($pid);
	unset($preset->pid);
	foreach ($preset->rules as $rid => $rule) {
		unset($preset->rules[$rid]->rid);
		unset($preset->rules[$rid]->pid);
	}
  ob_start();
  var_export($preset);
  $output = ob_get_contents();
  ob_end_clean();
  return str_replace('stdClass::__set_state', '', $output);
}

/**
 * Import/Export function. Import data.
 *
 * @param data
 *    The raw import data string.
 * @return
 *    The created preset.
 */
function wysiwyg_cleaner_preset_import($data) {
	$preset = eval('return '.$data.';');
	$preset = _wysiwyg_cleaner_array_to_object($preset);
	foreach ($preset->rules as $key => $rule) {
		$preset->rules[$key] = _wysiwyg_cleaner_array_to_object($preset->rules[$key]);
	}
  return wysiwyg_cleaner_preset_save($preset);
}

/**
 * Helper function. Convert an array to an object.
 *
 * @param array
 *    The array.
 * @return
 *    An object.
 */
function _wysiwyg_cleaner_array_to_object($array) {
	$object = new stdClass();
	if (is_array($array) && count($array) > 0) {
		foreach ($array as $name=>$value) {
			$name = strtolower(trim($name));
			if (!empty($name)) {
				$object->$name = $value;
			}
		}
	}
	return $object;
}

/**
 * Helper function. Get arguments informations ready for escaping.
 *
 * @param rule
 *    The rule object.
 * @return
 *    An array containing arguments informations ready for escaping.
 */
function _wysiwyg_cleaner_get_arguments($rule) {
	preg_match_all('/%[s|d|f|%]/', $rule->value, $matches);
  $arguments = array("");
  $cards = array('%s');
	if ($rule->arguments) {
  	$arguments = split(',', $rule->arguments);
  	$cards = $matches[0];
  	if (count($arguments) != count($cards)) {
  		$error = t('You must enter %count arguments.', array('%count' => count($cards)));
  		$arguments = array("");
  		$cards = array('%s');
  	}
  }
  $query_args = array_merge($arguments, array($rule->rule, $rule->value, $rule->pid, $rule->rid));
  return array('arguments' => $arguments, 'cards' => $cards, 'query_args' => $query_args, 'error' => $error);
}