
/**
 * @file
 * Wysiwyg javascript plugin.
 */

for (var i in wysiwyg_cleaner_presets) {

	var pid = wysiwyg_cleaner_presets[i];

	Drupal.wysiwyg.plugins['wysiwyg_cleaner_'+pid] = {
	
	  /**
	   * Execute the button.
	   */
	  invoke: function(data, settings, instanceId) {
	    if (data.format == 'html') {
	      var content = this._process(data.content, settings);
	    }
	    else if (data.format == 'placeholder') {
	    	var content = data.content;
	    }
	    if (typeof content != 'undefined') {
	      Drupal.wysiwyg.instances[instanceId].insert(content);
	    }
	  },
	  
	  /**
	   * Attach.
	   */
	  attach: function(content, settings, instanceId) {
	  	var editor = this;
	  	var instance = Drupal.wysiwyg.instances[instanceId];
	  	var config = Drupal.settings.wysiwyg.configs[instance.editor][instance.format];
	  	var iframe = $('iframe:not(.wysiwyg-cleaner-processed)', $('#'+instance.field).parent()).addClass('wysiwyg-cleaner-processed');
	  	
	  	if (!iframe.size()) {
	  		return content;
	  	}
	  	
	  	var iframeWindow = iframe[0].contentWindow;
	  	var iframeDocument = iframeWindow.document;
	  	var body = $('body', iframeDocument);
	  	
			if (config.paste_auto_cleanup_on_paste) {
				/*
				$(iframeDocument).bind('keydown',function(e) {
					var isMac = navigator.userAgent.indexOf('Mac') != -1;
					if (((isMac ? e.metaKey : e.ctrlKey) && e.keyCode == 86) || (e.shiftKey && e.keyCode == 45)) {
					}
				}
				*/
				$(iframeDocument).bind('input paste',function(e) {
					// After timeout, paste is effective
					window.setTimeout(function() {
						var contentAfterPaste = editor._process(body.html(), settings);
						body.html(contentAfterPaste);
					}, 0);
				});
			}

	    return content;
	  },
	  
	  /**
	   * Detach.
	   */
	  detach: function(content, settings, instanceId) {
	    return content;
	  },
	  
	  /**
	   * isNode.
	   */
	  isNode: function(node) {
	    return false;
	  },
	  
	  /**
	   * Process.
	   */
	  _process: function(content, settings) {
	  	// Create a temporary div. We have to do this for complex filters as li[attr*=value] to work.
			var temp = document.createElement('div');
			$(temp).attr('id', 'wysiwyg_cleaner_paste_temp').hide().html(content); //@todo : grab only pasted content
			$('body').append(temp);
	    Drupal.Wysiwyg.Cleaner($('#wysiwyg_cleaner_paste_temp'), settings['parameters']);
	    content = $(temp).html();
	    $(temp).remove();
	    return content;
	  }
	  
	};

}