<?php
// wysiwyg_cleaner.inc,v 1.1 2009/10/11 23:59:17 jide Exp

/**
 * @file
 * Wysiwyg plugin.
 */

/**
 * Implementation of hook_wysiwyg_plugin().
 *
 * The module seems to want "wysiwyg_cleaner_wysiwyg_cleaner_plugin"
 */
function wysiwyg_cleaner_wysiwyg_cleaner_plugin() {	
	$presets = wysiwyg_cleaner_presets_get_all();
  $plugins = array();
  $path = drupal_get_path('module', 'wysiwyg_cleaner');
  foreach ($presets as $preset) {
    $plugins['wysiwyg_cleaner_'.$preset->pid] = array(
	    'title' => t('Wysiwyg cleaner').' : '.$preset->name,
	    'js path' => $path . '/plugin',
	    'js file' => 'wysiwyg_cleaner.js',
	    'css file' => NULL,
	    'css path' => NULL,
	    'icon path' => $path . '/plugin',
	    'icon file' => 'wysiwyg_cleaner.png',
	    'icon title' => t('Clean up content').' : '.$preset->name,
	    'settings' => array(
	    	'path' => $path . '/plugin',
	    	'parameters' => $preset,
	    ),
    );
    if ($preset->icon) {
    	// Look in all possible folders
    	$file = array_shift(drupal_system_listing(basename($preset->icon), dirname($preset->icon), 'name', 0));
    	$plugins['wysiwyg_cleaner_'.$preset->pid]['icon path'] = dirname($file->filename);
    	$plugins['wysiwyg_cleaner_'.$preset->pid]['icon file'] = $file->basename;
    }
  }
	return $plugins;
}