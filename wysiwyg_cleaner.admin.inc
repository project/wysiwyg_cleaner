<?php
// wysiwyg_cleaner.admin.inc,v 1.1 2009/10/11 23:59:16 jide Exp

/**
 * @file
 * Admin functions.
 */

/**
 * Administration settings.
 */
function wysiwyg_cleaner_admin() {
  $presets = wysiwyg_cleaner_presets_get_all();
	$rows = array();
	
  if ($presets) {
	  foreach($presets as $pid => $preset) {
			$rows[] = array($preset->name, l('Edit', 'admin/settings/wysiwyg/cleaner/preset/'.$pid.'/edit').' '.l('Export', 'admin/settings/wysiwyg/cleaner/preset/'.$pid.'/export').' '.l('Delete', 'admin/settings/wysiwyg/cleaner/preset/'.$pid.'/delete'));
	  }
  }
  return theme('table', array(t('Name'), t('Operations')), $rows);
}

/**
 * Administration form. Add or edit a preset.
 */
function wysiwyg_cleaner_admin_preset_form(&$form_state, $pid = NULL) {

	drupal_add_js(drupal_get_path('module', 'wysiwyg_cleaner') .'/wysiwyg_cleaner.admin.js');
	drupal_add_css(drupal_get_path('module', 'wysiwyg_cleaner') .'/wysiwyg_cleaner.admin.css');
	
  $form = array();
  
	if ($pid) {
		$preset = wysiwyg_cleaner_preset_get($pid);
		$form['pid'] = array(
			'#type' => 'value',
			'#value' => $pid,
		);
	}
	
	$methods = variable_get('wysiwyg_cleaner_methods', array());
	$preview_markup = variable_get('wysiwyg_cleaner_preview', '');
	
  $form['#prefix'] = '<div id="wysiwyg_cleaner">';
	$form['#suffix'] = '</div>';
  
  $form['rules'] = array(
		'#type' => 'fieldset',
		'#tree' => TRUE,
		'#title' => t('Preset'),
  );
  
  $form['rules']['name'] = array(
		'#type' => 'textfield',
		'#title' => t('Title'),
		'#required' => TRUE,
		'#default_value' => $preset->name,
		'#description' => t("The preset title."),
  );
  
  $form['rules']['icon'] = array(
		'#type' => 'textfield',
		'#title' => t('Icon'),
		'#default_value' => $preset->icon,
		'#description' => t("Path to a button icon, e.g. sites/default/files/icon.png"),
  );
  
  $rule_form = array(
		'#tree' => TRUE,
		'#prefix' => '<div class="row">',
		'#suffix' => '</div>',
		'rule' => array(
			'#prefix' => '<div class="wrapper">',
			'#suffix' => '</div>',
			'#type' => 'textfield',
			'#title' => t('Rule'),
			'#size' => '35',
			'#attributes' => array('class' => 'rule'),
			'#description' => t('Enter a jquery selector or a regular expression. <br/>E.g. : <em>li > span[class*=\'title\']</em> or <em>/[a-z]+[0-9]*/g</em>.'),
  	),
		'value' => array(
			'#tree' => TRUE,
			'#prefix' => '<div class="wrapper">',
			'#suffix' => '</div>',
			'method' => array(
				'#type' => 'select',
				'#title' => t('Action'),
				'#attributes' => array('class' => 'method'),
				'#description' => t('Choose an action to apply.'),
				'#options' => $methods + array('custom' => t('<Custom>')),
			),
			'arguments' => array(
				'#prefix' => '<div class="wrapper-arguments">',
				'#suffix' => '</div>',
				'#type' => 'textfield',
				'#size' => 30,
				'#title' => t('Arguments'),
				'#attributes' => array('class' => 'arguments'),
				'#description' => t('Enter arguments, separated by commas, without quotes.'),
			),
			'custom' => array(
				'#prefix' => '<div class="custom">',
				'#suffix' => '</div>',
				'#type' => 'textarea',
				'#rows' => 1,
				'#description' => t('Enter a custom action.'),
			),
		),
		'delete' => array(
   		'#type' => 'markup',
   		'#value' => '<a class="link-delete-rule" style="display:none">X</a>',
		),
  );

  if ($preset) {
	  foreach($preset->rules as $rid => $rule) {
			$form['rules'][$rid] = $rule_form;
			$form['rules'][$rid]['rule']['#default_value'] = $rule->rule;
			// Predefined function
			if ($methods[$rule->value]) {
				$form['rules'][$rid]['value']['method']['#default_value'] = $rule->value;
				$form['rules'][$rid]['value']['arguments']['#default_value'] = $rule->arguments;
			}
			// Custom function
			else {
				$form['rules'][$rid]['value']['method']['#default_value'] = 'custom';
				$form['rules'][$rid]['value']['custom']['#default_value'] = $rule->value;
			}
	  }
  }

  for($i = 0; $i < variable_get('wysiwyg_cleaner_rows', 30); $i++) {
	  $form['rules']['new_'.$i] = $rule_form;
  }
  
  $form['rules']['link_add_rule'] = array(
   '#type' => 'markup',
   '#value' => '<a class="link-add-rule" style="display:none">'.t('Add a rule').'</a>',
  );

  $form['preview'] = array(
		'#tree' => TRUE,
  );
  
  $form['preview']['before'] = array(
		'#type' => 'fieldset',
		'#title' => t('Before'),
		'#value' => '<div class="wrapper">'.$preview_markup.'</div>',
		'#attributes' => array('class' => 'preview-before'),
  );
  
  $form['preview']['after'] = array(
		'#type' => 'fieldset',
		'#title' => t('After'),
		'#value' => '<div class="wrapper"></div>',
		'#attributes' => array('class' => 'preview-after'),
  );
  
  $form['preview']['link_edit_preview'] = array(
		'#type' => 'markup',
		'#value' => '<a class="link-edit-preview" style="display:none">'.t('Change preview markup').'</a>',
  );
  
  $form['preview']['link_switch_preview'] = array(
		'#type' => 'markup',
		'#value' => '<a class="link-switch-preview" style="display:none">'.t('Display markup').'</a>',
  );
  
  $form['preview']['edit'] = array(
		'#type' => 'textarea',
		'#default_value' => $preview_markup,
		'#description' => t('Click the preview button when you have finished.'),
		'#prefix' => '<div class="edit-preview">',
		'#suffix' => '</div>',
  );
  
  $form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
  );
  
  $form['preview_submit'] = array(
		'#type' => 'submit',
		'#value' => t('Preview'),
		'#attributes' => array('class' => 'wysiwyg_cleaner_preview_submit'),
  );

  return $form;
}

/**
 * Administration form submit function. Add or edit a preset.
 */
function wysiwyg_cleaner_admin_preset_form_validate($form, &$form_state) {
	foreach ($form_state['values']['rules'] as $key => $row) {
		if (is_array($row) && ((!is_numeric($key) && $row['rule']) || is_numeric($key))) {
			if ($row['value']['method'] != 'custom' && $row['value']['rule'] != '') {
				$rule = new stdClass();
				$rule->value = $row['value']['method'];
				$rule->arguments = $row['value']['arguments'];
				$arguments = _wysiwyg_cleaner_get_arguments($rule);
				// Error from the API : Count mismatch between cards and arguments.
				if ($arguments['error']) {
					form_set_error('rules]['.$key.'][value][arguments', $arguments['error']);
				}
				// Basic validation.
				foreach ($arguments['cards'] as $key => $card) {
					$argument = $arguments['argument'][$key];
					if (($card == '%d' || $card == '%f') && !is_numeric($argument)) {
						form_set_error('rules]['.$key.'][value][arguments', t('Argument number %position must be a number.', array('%position' => $key+1)));
					}
				}
				$cards = split(',', $arguments['cards']);
				$rule->arguments = $row['value']['arguments'];
			}
		}
	}
}

/**
 * Administration form submit function. Add or edit a preset.
 */
function wysiwyg_cleaner_admin_preset_form_submit($form, &$form_state) {

	variable_set('wysiwyg_cleaner_preview', $form_state['values']['preview']['edit']);

	// Add preset
	$preset = new stdClass();
	$preset->name = $form_state['values']['rules']['name'];
	$preset->icon = $form_state['values']['rules']['icon'];
	if ($form_state['values']['pid']) {
		$preset->pid = (int)$form_state['values']['pid'];
	}
	// Add rules
	foreach ($form_state['values']['rules'] as $key => $row) {
		// Loop if either it is a new rule and it *has* a value,
		// either it is an existing rule with or without a value, because we need to 
		// keep this rule to further delete it.
		if (is_array($row) && ((!is_numeric($key) && $row['rule']) || is_numeric($key))) {
			$rule = new stdClass();
			$rule->rule = $row['rule'];
			if ($row['value']['method'] == 'custom') {
				$rule->value = $row['value']['custom'];
				$rule->arguments = '';
			}
			else {
				$rule->value = $row['value']['method'];
				$rule->arguments = $row['value']['arguments'];
			}
			if (is_numeric($key)) {
				$rule->rid = $key;
			}
			$preset->rules[] = $rule;
		}
	}
	$preset = wysiwyg_cleaner_preset_save($preset);
  drupal_set_message(t('Preset saved.'));
  $form_state['redirect'] = 'admin/settings/wysiwyg/cleaner';
}

/**
 * Administration form. Delete a preset.
 */
function wysiwyg_cleaner_admin_preset_delete_form(&$form_state, $pid = NULL) {
  
  $form = array();
  
	if ($pid) {
		$preset = wysiwyg_cleaner_preset_get($pid);
		$form['pid'] = array(
			'#type' => 'value',
			'#value' => $pid,
		);
	}
	else {
    drupal_set_message(t('The specified preset was not found'), 'error');
    drupal_goto('admin/settings/wysiwyg/cleaner');
	}

  return confirm_form(
    $form,
    t('Are you sure you want to delete the preset %preset?', array('%preset' => $preset->name)), 'admin/settings/wysiwyg/cleaner',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Administration form submit function. Delete a preset.
 */
function wysiwyg_cleaner_admin_preset_delete_form_submit($form, &$form_state) {
  $pid = (int)$form_state['values']['pid'];
  wysiwyg_cleaner_preset_delete($pid);
  drupal_set_message(t('Preset deleted.'));
  $form_state['redirect'] = 'admin/settings/wysiwyg/cleaner';
}

/**
 * Administration form. Export a preset.
 */
function wysiwyg_cleaner_admin_preset_export_form(&$form_state, $pid = NULL) {
	
  $form = array();
  
  $form['export'] = array(
  	'#type' => 'textarea',
  	'#title' => t('Export a preset'),
  	'#value' => wysiwyg_cleaner_preset_export($pid),
  	'#rows' => 20,
  );
	
  return $form;
}

/**
 * Administration form. Import a preset.
 */
function wysiwyg_cleaner_admin_import_form(&$form_state) {
	
  $form = array();
  
  $form['import'] = array(
  	'#type' => 'textarea',
  	'#title' => t('Paste the preset to import'),
  	'#rows' => 20,
  );
  
  $form['submit'] = array(
		'#type' => 'submit',
		'#value' => t('Save'),
  );
	
  return $form;
}

/**
 * Administration form submit function. Import a preset.
 */
function wysiwyg_cleaner_admin_import_form_submit($form, &$form_state) {
  $data = $form_state['values']['import'];
  wysiwyg_cleaner_preset_import($data);
  drupal_set_message(t('Preset imported.'));
  $form_state['redirect'] = 'admin/settings/wysiwyg/cleaner';
}